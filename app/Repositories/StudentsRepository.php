<?php

namespace App\Repositories;

use App\Http\Requests\StudentsRequest;
use Illuminate\Database\Eloquent\Model;

use App\Models\Students;


/**
 * App\Repositories\StudentsRepository
 *
 * @property int $student_id
 * @property int $class_id
 * @property string $last_name
 * @property string $first_name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\StudentsRepository whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\StudentsRepository whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\StudentsRepository whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\StudentsRepository whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\StudentsRepository whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\StudentsRepository whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class StudentsRepository extends Model
{
//    use SearchableTrait;

    protected $model = Students::class;

    const PRIMARY_KEY = 'student_id';
    const TABLE_NAME = 'students';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::PRIMARY_KEY;

    /**
     * Get model Students
     * @return Students
     */
    public function model(): Students
    {
        return new $this->model();
    }

    /**
     * Create Student.
     *
     * @param StudentsRequest $request
     * @return Students
     */
    public function createStudent(StudentsRequest $request): Students
    {
        $student = new Students;
        $student->setClassId($request->input('class_id'));
        $student->setLastName($request->input('last_name'));
        $student->setFirstName($request->input('first_name'));
        return $student;
    }

    /**
     * Update Student.
     *
     * @param StudentsRequest $request
     * @param Students $student
     * @return Students
     */
    public function updateStudent(StudentsRequest $request, Students $student): Students
    {
        $student->setClassId($request->input('class_id'));
        $student->setLastName($request->input('last_name'));
        $student->setFirstName($request->input('first_name'));
        return $student;
    }

}