<?php

namespace App\Repositories;

use App\Http\Requests\SchoolsRequest;
use Illuminate\Database\Eloquent\Model;

//use App\Traits\SearchableTrait;
use App\Models\Schools;

/**
 * App\Repositories\SchoolsRepository
 *
 * @property int $school_id
 * @property string $school_name
 * @property string $city
 * @property string $address
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\SchoolsRepository whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\SchoolsRepository whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\SchoolsRepository whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\SchoolsRepository whereSchoolId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\SchoolsRepository whereSchoolName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\SchoolsRepository whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SchoolsRepository extends Model
{
    protected $model = Schools::class;

    const TABLE_NAME = 'schools';
    const PRIMARY_KEY = 'school_id';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::PRIMARY_KEY;


    /**
     * Get model Schools
     * @return Schools
     */
    public function model(): Schools
    {
        return new $this->model();
    }

    /**
     * Create School.
     *
     * @param SchoolsRequest $request
     * @return Schools
     */
    public function createSchool(SchoolsRequest $request): Schools
    {
        $school = new Schools;
        $school->setSchoolName($request->input('school_name'));
        $school->setCity($request->input('city'));
        $school->setAddress($request->input('address'));

        return $school;
    }

    /**
     * Update School.
     *
     * @param SchoolsRequest $request
     * @param Schools $school
     * @return Schools
     */
    public function updateSchool(SchoolsRequest $request, Schools $school): Schools
    {
        $school->setSchoolName($request->input('school_name'));
        $school->setCity($request->input('city'));
        $school->setAddress($request->input('address'));
        return $school;
    }
}