<?php

namespace App\Repositories;

use App\Http\Requests\ClassesRequest;
use Illuminate\Database\Eloquent\Model;

//use App\Traits\SearchableTrait;
use App\Models\Classes;


/**
 * App\Repositories\ClassesRepository
 *
 * @property int $class_id
 * @property int $school_id
 * @property int $year
 * @property int $ordinal
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\ClassesRepository whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\ClassesRepository whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\ClassesRepository whereOrdinal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\ClassesRepository whereSchoolId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\ClassesRepository whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Repositories\ClassesRepository whereYear($value)
 * @mixin \Eloquent
 */
class ClassesRepository extends Model
{

    protected $model = Classes::class;

    const TABLE_NAME = 'classes';
    const PRIMARY_KEY = 'class_id';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::PRIMARY_KEY;

    /**
     * Get model Classes
     * @return Classes
     */
    public function model(): Classes
    {
        return new $this->model();
    }

    /**
     * Create class.
     *
     * @param ClassesRequest $request
     * @return Classes
     */
    public function createClass(ClassesRequest $request): Classes
    {
        $class = new Classes;
        $class->setSchoolId($request->input('school_id'));
        $class->setYear($request->input('year'));
        $class->setOrdinal($request->input('ordinal'));

        return $class;
    }

    /**
     * Update class.
     *
     * @param ClassesRequest $request
     * @param Classes $class
     * @return Classes
     */
    public function updateClass(ClassesRequest $request, Classes $class): Classes
    {
        $class->setSchoolId($request->input('school_id'));
        $class->setYear($request->input('year'));
        $class->setOrdinal($request->input('ordinal'));
        return $class;
    }
}