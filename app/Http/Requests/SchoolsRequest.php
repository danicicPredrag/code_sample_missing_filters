<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

use App\Models\Students;
use App\Models\Schools;
use App\Models\Classes;

class SchoolsRequest extends FormRequest
{
    protected $model = Schools::class;

    private $schools;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $this->schools = Schools::find($this->route('id'));

        if ($this->schools instanceof Schools) {
            return $this->getRulesForUpdate();
        } else {
            return $this->getRulesForCreate();
        }
    }

    /**
     *  Get rules for create validation.
     * @return array
     */
    private function getRulesForCreate(): array
    {
        return [
            'school_name' => 'required|string|max:255',
            'city' => 'required|string|max:255',
            'address' => 'required|string|max:255',
        ];
    }

    /**
     *  Get rules for update validation.
     * @return array
     */
    private function getRulesForUpdate(): array
    {
        return [
            'school_name' => 'required|string|max:255',
            'city' => 'required|string|max:255',
            'address' => 'required|string|max:255',
        ];
    }

    /**
     * Override method failedValidation.
     *  Important: This method has to be override for custom validation.
     *
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json(['message' => trans('messages.errors.validation'), 'data' => $validator->messages()], 400));
    }
}
