<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

use App\Models\Students;
use App\Models\Schools;
use App\Models\Classes;

class StudentsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $this->classes = Students::find($this->route('id'));

        if ($this->classes instanceof Students) {
            return $this->getRulesForUpdate();
        } else {
            return $this->getRulesForCreate();
        }
    }

    /**
     *  Get rules for create validation.
     * @return array
     */
    private function getRulesForCreate(): array
    {
        return [
            'class_id' => 'required|exists:' . Classes::TABLE_NAME . ',' . Classes::PRIMARY_KEY ,
            'last_name' => 'required|max:255',
            'first_name' => 'required|max:255',
        ];
    }

    /**
     *  Get rules for update validation.
     * @return array
     */
    private function getRulesForUpdate(): array
    {
        return [
            'class_id' => 'required|exists:' . Classes::TABLE_NAME . ',' . Classes::PRIMARY_KEY ,
            'last_name' => 'required|max:255',
            'first_name' => 'required|max:255',
        ];
    }

    /**
     * Override method failedValidation.
     *  Important: This method has to be override for custom validation.
     *
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json(['message' => trans('messages.errors.validation'), 'data' => $validator->messages()], 400));
    }
}
