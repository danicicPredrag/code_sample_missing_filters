<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

use App\Models\Classes;
use App\Models\Schools;

class ClassesRequest extends FormRequest
{

    protected $model = Classes::class;

    private $classes;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $this->classes = Classes::find($this->route('id'));

        if ($this->classes instanceof Classes) {
            return $this->getRulesForUpdate();
        } else {
            return $this->getRulesForCreate();
        }
    }

    /**
     *  Get rules for create validation.
     * @return array
     */
    private function getRulesForCreate(): array
    {
        return [
            'school_id' => 'required|exists:' . Schools::TABLE_NAME . ',' . Schools::PRIMARY_KEY ,
            'year' => [
                'required',
                'integer',
                'min:1',
                'max:8',
                'unique_with:'. Classes::TABLE_NAME . ',ordinal,school_id',
            ],
            'ordinal' => [
                'required',
                'integer',
                'min:1',
                'max:8',
            ],
        ];
    }

    /**
     *  Get rules for update validation.
     * @return array
     */
    private function getRulesForUpdate(): array
    {
        return [
            'school_id' => 'required|exists:' . Schools::TABLE_NAME . ',' . Schools::PRIMARY_KEY ,
            'year' => [
                'required',
                'integer',
                'min:1',
                'max:8',
                'unique_with:'. Classes::TABLE_NAME . ',ordinal,school_id',
            ],
            'ordinal' => [
                'required',
                'integer',
                'min:1',
                'max:8',
            ],
        ];
    }

    /**
    * Override method failedValidation.
    *  Important: This method has to be override for custom validation.
    *
    * @param Validator $validator
    */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json(['message' => trans('messages.errors.validation'), 'data' => $validator->messages()], 400));
    }

}
