<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\StudentsRequest;
use App\Repositories\StudentsRepository;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use App\Models\Students;

class StudentsController extends Controller
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        $students = Students::paginate(config('constants.pagination.students_per_page'));

        return response()->json($students, 200);
    }


    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id): JsonResponse
    {

        $student = Students::with(['studentClass' => function ($query) {
            $query->with('school');
        }])->find($id);

        if (empty($student)) {
            return response()->json(['message' => trans('messages.errors.not_found_student')], 400);
        }

        return response()->json($student, 200);

    }


    /**
     * Create new Student
     *
     * @param StudentsRepository $studentsRepository
     * @param StudentsRequest $request
     * @return JsonResponse
     */
    public function create(StudentsRepository $studentsRepository, StudentsRequest $request): JsonResponse
    {

        $student = $studentsRepository->createStudent($request);

        if (!$student->save()) {
            return response()->json(['message' => trans('messages.errors.saving_student')], 400);
        }

        return response()->json(['message' => trans('messages.messages.added_new_student')], 200);

    }


    /**
     * Update  exissting Student
     * @param StudentsRepository $studentsRepository
     * @param StudentsRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(StudentsRepository $studentsRepository, StudentsRequest $request, int $id): JsonResponse
    {
        $student = $studentsRepository->model()->find($id);
        if (empty($student)) {
            return response()->json(['message' => trans('messages.errors.not_found_student')], 400);
        }

        $student = $studentsRepository->updateStudent($request, $student);

        if (!$student->save()) {
            return response()->json(['message' => trans('messages.errors.updating_student')], 400);
        }

        return response()->json(['message' => trans('messages.messages.updated_student')], 200);

    }

    /**
     * Student Deleted
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function delete($id)
    {
        $student = Students::find($id);
        if (empty($student)) {
            return response()->json(['message' => trans('messages.errors.not_found_student')], 400);
        }

        $student->delete();

        return response()->json(['message' => trans('messages.messages.deleted_student')], 200);

    }
}
