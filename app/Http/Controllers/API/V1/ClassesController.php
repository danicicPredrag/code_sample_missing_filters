<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\ClassesRequest;
use App\Repositories\ClassesRepository;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;


use App\Models\Classes;
use App\Models\Students;

class ClassesController extends Controller
{
    /**
     * Display list of Classes.
     *
     * @Method GET
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $classes = Classes::paginate(config('constants.pagination.classes_per_page'));

        return response()->json($classes, 200);

    }

    /**
     * Display single Class
     *
     * @Method GET
     * @param $id
     * @return JsonResponse
     */
    public function get($id): JsonResponse
    {
        $class = Classes::with(['school', 'students'])->find($id);

        if (empty($class)) {
            return response()->json(['message' => trans('messages.errors.not_found_class')], 400);
        }

        return response()->json($class, 200);

    }

    /**
     * Create new Class
     *
     * @Method POST
     * @param ClassesRepository $classesRepository
     * @param ClassesRequest $request
     * @return JsonResponse
     */
    public function create(ClassesRepository $classesRepository, ClassesRequest $request): JsonResponse
    {

        $class = $classesRepository->createClass($request);

        if (!$class->save()) {
            return response()->json(['message' => trans('messages.errors.saving_class')], 400);
        }

        return response()->json(['message' => trans('messages.messages.added_new_class')], 200);
    }

    /**
     * Update existing Class
     *
     * @Method POST
     * @param ClassesRepository $classesRepository
     * @param ClassesRequest $request
     * @return JsonResponse
     */
    public function update(ClassesRepository $classesRepository, ClassesRequest $request, int $id): JsonResponse
    {
        $class = $classesRepository->model()->find($id);
        if (empty($class)) {
            return response()->json(['message' => trans('messages.errors.not_found_class')], 400);
        }

        $class = $classesRepository->updateClass($request, $class);

        if (!$class->save()) {
            return response()->json(['message' => trans('messages.errors.saving_class')], 400);
        }

        return response()->json(['message' => trans('messages.messages.updated_class')], 200);

    }

    public function delete($id)
    {
        $class = Classes::with('students')->find($id);

        if (empty($class)) {
            return response()->json(['message' => trans('messages.errors.not_found_student')], 400);
        }

        if (!empty($class->students())) {
            Students::where(Classes::PRIMARY_KEY, $id)->delete();
        }

        $class->delete();

        return response()->json(['message' => trans('messages.messages.deleted_class')], 200);
    }

}
