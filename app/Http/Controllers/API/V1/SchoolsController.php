<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\SchoolsRequest;
use App\Repositories\SchoolsRepository;
use Illuminate\Http\JsonResponse;

use App\Http\Controllers\Controller;

use App\Models\Schools;

class SchoolsController extends Controller
{
    /**
     * get Schools
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $schools = Schools::paginate(config('constants.pagination.schools_per_page'));

        return response()->json($schools, 200);
    }

    /**
     * get single School
     *
     * @param $id
     * @return JsonResponse
     */
    public function get($id): JsonResponse
    {

        $school = Schools::with('classes')->find($id);

        if (empty($school)) {
            return response()->json(['message' => trans('messages.errors.not_found_school')], 400);
        }

        return response()->json($school, 200);

    }

    /**
     * Create new School
     *
     * @param SchoolsRepository $schoolsRepository
     * @param SchoolsRequest $request
     * @return JsonResponse
     */
    public function create(SchoolsRepository $schoolsRepository, SchoolsRequest $request): JsonResponse
    {
        $school = $schoolsRepository->createSchool($request);

        if (!$school->save()) {
            return response()->json(['message' => trans('messages.errors.saving_school')], 400);
        }

        return response()->json(['message' => trans('messages.messages.added_new_school')], 200);
    }

    /**
     * Update existing Class
     *
     * @Method POST
     * @param SchoolsRepository $schoolsRepository
     * @param SchoolsRequest $request
     * @return JsonResponse
     */
    public function update(SchoolsRepository $schoolsRepository, SchoolsRequest $request, int $id ): JsonResponse
    {

        $school = $schoolsRepository->model()->find($id);
        if (empty($school)) {
            return response()->json(['message' => trans('messages.errors.not_found_school')], 400);
        }

        $school = $schoolsRepository->updateSchool($request, $school);

        if (!$school->save()) {
            return response()->json(['message' => trans('messages.errors.saving_school')], 400);
        }

        return response()->json(['message' => trans('messages.messages.updated_school')], 200);
    }

    /**
     * Delete existing school
     * @param $id
     * @return JsonResponse
     */
    public function delete($id)
    {
        $school = Schools::with('classes')->find($id);

        if (!empty($school->classes)) {
            return response()->json(['message' => trans('messages.errors.school_have_classes')], 400);
        }

        $school->delete();

        return response()->json(['message' => trans('messages.messages.deleted_school')], 200);
    }

}

