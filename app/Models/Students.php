<?php

namespace App\Models;

use App\Repositories\StudentsRepository;



/**
 * App\Models\Students
 *
 * @property-read \App\Models\Classes $studentClass
 * @mixin \Eloquent
 * @property int $student_id
 * @property int $class_id
 * @property string $last_name
 * @property string $first_name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Students whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Students whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Students whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Students whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Students whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Students whereUpdatedAt($value)
 */
class Students extends StudentsRepository
{
    /**
     * Get student_id
     * @return int
     */
    public function getStudentId(): int
    {
        return $this->student_id;
    }

    /**
     * Get class_id
     * @return int
     */
    public function getClassId(): int
    {
        return $this->class_id;
    }

    /**
     * Set class_id
     * @param int $class_id
     */
    public function setClassId(int $class_id): void
    {
        $this->class_id = $class_id;
    }

    /**
     * Get last_name
     * @return string
     */
    public function getLastName(): string
    {
        return $this->last_name;
    }

    /**
     * Set last_name
     * @param string $last_name
     */
    public function setLastName(string $last_name): void
    {
        $this->last_name = $last_name;
    }

    /**
     * Get first_name
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->first_name;
    }

    /**
     * Set first_name
     * @param string $first_name
     */
    public function setFirstName(string $first_name): void
    {
        $this->first_name = $first_name;
    }


    public function studentClass() {
        return $this->belongsTo(Classes::class,Classes::PRIMARY_KEY, Classes::PRIMARY_KEY);
    }
}
