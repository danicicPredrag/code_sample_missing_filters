<?php

namespace App\Models;

use App\Repositories\SchoolsRepository;

/**
 * App\Models\Schools
 *
 * @property int $school_id
 * @property string $school_name
 * @property string $city
 * @property string $address
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Classes[] $classes
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schools whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schools whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schools whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schools whereSchoolId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schools whereSchoolName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schools whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Schools extends SchoolsRepository
{
    /** Get school_id
     * @return int
     */
    public function getSchoolId(): int
    {
        return $this->school_id;
    }

    /** Get school_name
     * @return string
     */
    public function getSchoolName(): string
    {
        return $this->school_name;
    }

    /**
     * Set school_name
     * @param string $school_name
     */
    public function setSchoolName(string $school_name): void
    {
        $this->school_name = $school_name;
    }

    /** Get city
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * Set city
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /** Get address
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * Set address
     * @param string $address
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    public function classes()
    {
        return $this->hasMany(Classes::class, self::PRIMARY_KEY, self::PRIMARY_KEY);
    }


}
