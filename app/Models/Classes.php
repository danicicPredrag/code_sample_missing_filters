<?php

namespace App\Models;

use Felixkiss;

use App\Repositories\ClassesRepository;


/**
 * App\Models\Classes
 *
 * @property int $class_id
 * @property int $school_id
 * @property int $year
 * @property int $ordinal
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Schools $school
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Students[] $students
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Classes whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Classes whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Classes whereOrdinal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Classes whereSchoolId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Classes whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Classes whereYear($value)
 * @mixin \Eloquent
 */
class Classes extends ClassesRepository
{


    protected $model = Classes::class;

    /**
     * Get class_id
     * @return int
     */
    public function getClassId(): int
    {
        return $this->class_id;
    }

    /**
     * Get school_id
     * @return int
     */
    public function getSchoolId(): int
    {
        return $this->school_id;
    }

    /**
     * Set school_id
     * @param int $school_id
     */
    public function setSchoolId(int $school_id): void
    {
        $this->school_id = $school_id;
    }

    /**
     * Get year
     * @return int
     */
    public function getYear(): int
    {
        return $this->year;
    }

    /**
     * Set year
     * @param int $year
     */
    public function setYear(int $year): void
    {
        $this->year = $year;
    }

    /**
     * Get ordinal
     * @return int
     */
    public function getOrdinal(): int
    {
        return $this->ordinal;
    }

    /**
     * Set ordinal
     * @param int $ordinal
     */
    public function setOrdinal(int $ordinal): void
    {
        $this->ordinal = $ordinal;
    }

    public function school()
    {
        return $this->belongsTo(Schools::class, Schools::PRIMARY_KEY, Schools::PRIMARY_KEY);
    }

    public function students()
    {
        return $this->hasMany(Students::class, self::PRIMARY_KEY, self::PRIMARY_KEY);
    }

}
