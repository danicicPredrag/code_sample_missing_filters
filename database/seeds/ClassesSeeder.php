<?php

use Illuminate\Database\Seeder;

use App\Classes;
use App\Schools;

class ClassesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $schools = Schools::get();
        foreach ($schools as $school) {
            for ($year = 1; $year < 9; $year++) {
                $ordinal_total = rand(5,8);
                for ($ordinal = 1; $ordinal < $ordinal_total; $ordinal++) {
                    $class = new Classes();
                    $class->school_id = $school->{Schools::PRIMARY_KEY};
                    $class->year	 = $year;
                    $class->ordinal	 = $ordinal;
                    $class->save();
                }
            }

        }
    }
}
