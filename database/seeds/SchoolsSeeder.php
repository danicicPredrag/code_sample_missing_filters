<?php

use Faker\Factory;
use Illuminate\Database\Seeder;

use App\Schools;

class SchoolsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for ($i = 0; $i < 20; $i++) {
            $school = new Schools();
            $school->school_name = $faker->firstName() . ' ' . $faker->lastName;
            $school->city = $faker->city;
            $school->address = $faker->address;
            $school->save();
        }


    }
}
