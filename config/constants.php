<?php

return array(
    'pagination' => array(
        'schools_per_page' => 10,
        'classes_per_page' => 25,
        'students_per_page' => 25,
    ),
);