<?php

return [
    'errors' => [

        'not_found_school' => 'school not found',
        'not_found_class' => 'class not found',
        'not_found_student' => 'student not found',

        'saving_school' => 'error saving school',
        'saving_class' => 'error saving class',
        'saving_student' => 'error saving student',

        'updating_school' => 'error updating school',
        'updating_class' => 'error updating class',
        'updating_student' => 'error updating student',

        'school_have_classes' => 'School have classes can not delete it',

    ],

    'messages' => [
        'added_new_school' => 'new school added',
        'added_new_class' => 'new class added',
        'added_new_student' => 'new student added',

        'updated_school' => 'updated school',
        'updated_class' => 'updated class',
        'updated_student' => 'updated student',

        'deleted_school' => 'deleted school',
        'deleted_class' => 'deleted class',
        'deleted_student' => 'deleted student',


    ],


];
