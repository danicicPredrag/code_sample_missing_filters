<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::prefix('V1')->group(function () {
    Route::get('schools-index', 'API\V1\SchoolsController@index')->name('getSingleSchool');
    Route::get('schools-get/{id}', 'API\V1\SchoolsController@get')->name('getSchool');
    Route::post('schools-create', 'API\V1\SchoolsController@create')->name('createSchool');
    Route::post('schools-update/{id}', 'API\V1\SchoolsController@update')->name('updateSchool');
    Route::delete('schools-delete/{id}', 'API\V1\SchoolsController@delete')->name('deleteSchool');


    Route::get('classes-index', 'API\V1\ClassesController@index')->name('getSingleClass');
    Route::get('classes-get/{id}', 'API\V1\ClassesController@get')->name('getClass');
    Route::post('classes-create', 'API\V1\ClassesController@create')->name('createClass');
    Route::post('classes-update/{id}', 'API\V1\ClassesController@update')->name('updateClass');
    Route::delete('classes-delete/{id}', 'API\V1\ClassesController@delete')->name('deleteClass');

    Route::get('students-index', 'API\V1\StudentsController@index')->name('getSingleStudent');
    Route::get('students-get/{id}', 'API\V1\StudentsController@get')->name('getStudent');
    Route::post('students-create', 'API\V1\StudentsController@create')->name('createStudent');
    Route::post('students-update/{id}', 'API\V1\StudentsController@update')->name('updateStudent');
    Route::delete('students-delete/{id}', 'API\V1\StudentsController@delete')->name('deleteStudent');


});